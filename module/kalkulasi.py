from sympy import fourier_series
from sympy import pi
from sympy import plot

from sympy.abc import x

def kalkulasi_deret_fourier(freq_1, freq_2):

    f = x ** 3

    s = fourier_series(f, (x, -pi, pi))

    s1 = s.truncate(int(freq_1))
    s2 = s.truncate(int(freq_2))

    p = plot(f, s1, s2, (x, -pi, pi), show=False, legend=True)

    p[0].line_color = (0, 0, 0)
    p[0].label = 'x**3'

    p[1].line_color = (0.25, 0.25, 0.25)
    p[1].label = f'Frekuensi 1 = {freq_1}'

    p[2].line_color = (0.55, 0.55, 0.55)
    p[2].label = f'Frekuensi 2 = {freq_2}'

    p.show()

    return f"Frekuensi 1: {s1}\n\nFrekuensi 2: {s2}"
