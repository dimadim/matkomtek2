import tkinter.messagebox as tm
import tkinter

from module.kalkulasi import kalkulasi_deret_fourier

def show():
    tm.showinfo(title="Hasil", message=kalkulasi_deret_fourier(freq_1.get(), freq_2.get()))

root = tkinter.Tk()

tkinter.Label(root, text='Persamaan: f(x) = x**3').grid(row=0)
tkinter.Label(root, text='Frekuensi 1: ').grid(row=1)
tkinter.Label(root, text='Frekuensi 2: ').grid(row=2)

freq_1 = tkinter.Entry(root)
freq_2 = tkinter.Entry(root)

freq_1.grid(row=1, column=1)
freq_2.grid(row=2, column=1)

tkinter.Button(root, text="Kalkulasi", command=show).grid(row=3, column=1)

tkinter.mainloop()